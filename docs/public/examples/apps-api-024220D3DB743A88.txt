IQRF gateway 1
==============

API docs
--------

- https://docs.iqrf.org/iqrf-gateway/api.html
- https://docs.iqrf.org/iqrf-gateway/api-beta.html

MQTT topics
-----------

- gateway/024220D3DB743A88/commands (IQRF JSON requests)
- gateway/024220D3DB743A88/events (IQRF JSON responses)

1) Check connection to DPA coordinator

Request
-------

{
  "mType": "iqrfEmbedOs_Read",
  "data": {
    "msgId": "testEmbedOs",
    "req": {
      "nAdr": 0,
      "param": {}
    },
    "returnVerbose": false
  }
}

Response
--------

{
    "mType": "iqrfEmbedOs_Read",
    "data": {
        "msgId": "testEmbedOs",
        "rsp": {
            "nAdr": 0,
            "hwpId": 0,
            "rCode": 0,
            "dpaVal": 0,
            "result": {
                "mid": 2165429211,
                "osVersion": 67,
                "trMcuType": 180,
                "osBuild": 2248,
                "rssi": 0,
                "supplyVoltage": 3.3053164556962026,
                "flags": 0,
                "slotLimits": 117,
                "ibk": [
                    196,
                    105,
                    188,
                    28,
                    123,
                    43,
                    69,
                    50,
                    233,
                    30,
                    176,
                    24,
                    172,
                    57,
                    126,
                    166
                ],
                "dpaVer": 1043,
                "perNr": 0,
                "embeddedPers": [
                    0,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    9,
                    13
                ],
                "hwpid": 0,
                "hwpidVer": 0,
                "flagsEnum": 5,
                "userPer": []
            }
        },
        "status": 0,
        "insId": "iqdgw01-024220d3db743a88"
    }
}

2) Write TX power, RX filter, RF channel and access password to DPA coordinator

Request
-------

{
  "mType": "iqmeshNetwork_WriteTrConf",
  "data": {
    "msgId": "testIqmeshWrite",
    "repeat": 1,
    "req": {
      "deviceAddr": 0,
      "txPower": 7,
      "rxFilter": 7,
      "rfChannelA": 42,
      "accessPassword": "Iqrf1"
    },
    "returnVerbose": false
  }
}

Response
--------

{
    "mType": "iqmeshNetwork_WriteTrConf",
    "data": {
        "msgId": "testIqmeshWrite",
        "rsp": {
            "deviceAddr": 0,
            "writeSuccess": true,
            "restartNeeded": true
        },
        "status": 0,
        "statusStr": "ok",
        "insId": "iqdgw01-024220d3db743a88"
    }
}

3) Restart DPA coordinator

Request
-------

{
  "mType": "iqrfEmbedOs_Restart",
  "data": {
    "msgId": "testEmbedOs",
    "req": {
      "nAdr": 0,
      "param": {}
    },
    "returnVerbose": false
  }
}

Responses
---------

{
    "mType": "iqrfEmbedOs_Restart",
    "data": {
        "msgId": "testEmbedOs",
        "rsp": {
            "nAdr": 0,
            "hwpId": 0,
            "rCode": 0,
            "dpaVal": 0,
            "result": {}
        },
        "status": 0,
        "insId": "iqdgw01-024220d3db743a88"
    }
}

{
    "mType": "iqrfRaw",
    "data": {
        "msgId": "async",
        "rsp": {
            "rData": "00.00.ff.3f.00.00.80.00.13.04.00.fd.22.00.00.00.00.00.00.05"
        },
        "status": 0,
        "insId": "iqdgw01-024220d3db743a88"
    }
}

4) Bond devices to the network using AutoNetwork

Request
-------

{
  "mType": "iqmeshNetwork_AutoNetwork",
  "data": {
    "msgId": "testIqmeshAutonetwork",
    "req": {
      "discoveryTxPower": 6,
      "discoveryBeforeStart": false,
      "actionRetries": 1,
      "stopConditions": {
        "waves": 2,
        "emptyWaves": 1
      }
    },
    "returnVerbose": false
  }
}

Responses
---------

...progress messages

{
    "mType": "iqmeshNetwork_AutoNetwork",
    "data": {
        "msgId": "testIqmeshAutonetwork",
        "rsp": {
            "wave": 1,
            "nodesNr": 1,
            "newNodesNr": 1,
            "waveStateCode": 16,
            "progress": 100,
            "lastWave": false,
            "newNodes": [
                {
                    "mid": "81112f11",
                    "address": 1
                }
            ]
        },
        "status": 0,
        "statusStr": "ok",
        "insId": "iqdgw01-024220d3db743a88"
    }
}

...progress messages

{
    "mType": "iqmeshNetwork_AutoNetwork",
    "data": {
        "msgId": "testIqmeshAutonetwork",
        "rsp": {
            "wave": 2,
            "nodesNr": 1,
            "newNodesNr": 0,
            "waveStateCode": 12,
            "progress": 100,
            "lastWave": true
        },
        "status": 0,
        "statusStr": "ok",
        "insId": "iqdgw01-024220d3db743a88"
    }
}

5) Run network enumeration into daemon's DB (DB must be enabled first via webapp/config/iqrf-info)

Requests
--------

{
  "mType": "infoDaemon_Enumeration",
  "data": {
    "msgId": "testInfoEnum",
    "req": {
      "command": "now"
    },
    "returnVerbose": false
  }
}

Responses
---------

...progress messages

{
    "mType": "infoDaemon_Enumeration",
    "data": {
        "msgId": "testInfoEnum",
        "rsp": {
            "command": "now",
            "enumPhase": 5,
            "step": 1,
            "steps": 1,
            "percentage": 100
        },
        "insId": "iqdgw01-024220d3db743a88",
        "statusStr": "ok",
        "status": 0
    }
}

6) Check standard sensors and binary outputs in daemon's DB

Requests
--------

{
  "mType": "infoDaemon_GetSensors",
  "data": {
    "msgId": "GetSensors",
    "req": {},
    "returnVerbose": false
  }
}

{
  "mType": "infoDaemon_GetBinaryOutputs",
  "data": {
    "msgId": "GetBinaryOutputs",
    "req": {},
    "returnVerbose": false
  }
}

Responses
---------

{
    "mType": "infoDaemon_GetSensors",
    "data": {
        "msgId": "GetSensors",
        "rsp": {
            "sensorDevices": [
                {
                    "nAdr": 1,
                    "sensors": [
                        {
                            "idx": 0,
                            "id": "TEMPERATURE",
                            "type": 1,
                            "name": "Temperature",
                            "shortName": "T",
                            "unit": "°C",
                            "decimalPlaces": 4,
                            "frcs": [
                                144,
                                224
                            ]
                        },
                        {
                            "idx": 1,
                            "id": "BINARYDATA7",
                            "type": 129,
                            "name": "Binary data7",
                            "shortName": "bin7",
                            "unit": "?",
                            "decimalPlaces": 0,
                            "frcs": [
                                16,
                                144
                            ]
                        },
                        {
                            "idx": 2,
                            "id": "BINARYDATA7",
                            "type": 129,
                            "name": "Binary data7",
                            "shortName": "bin7",
                            "unit": "?",
                            "decimalPlaces": 0,
                            "frcs": [
                                16,
                                144
                            ]
                        }
                    ]
                }
            ]
        },
        "status": 0,
        "insId": "iqdgw01-024220d3db743a88"
    }
}

{
    "mType": "infoDaemon_GetBinaryOutputs",
    "data": {
        "msgId": "test",
        "rsp": {
            "binOutDevices": [
                {
                    "nAdr": 1,
                    "binOuts": 2
                }
            ]
        },
        "status": 0,
        "insId": "iqdgw01-024220d3db743a88"
    }
}

7) Schedule tasks to read standard temperature sensor every 180s and put it into sleep for 170s

Request
-------

{
  "mType": "mngScheduler_AddTask",
  "data": {
    "msgId": "SchedulerAdd",
    "req": {
      "clientId": "SchedulerMessaging",
      "task": [
        {
          "messaging": "MqttMessaging",
          "message": {
            "mType": "iqrfSensor_ReadSensorsWithTypes",
            "data": {
              "msgId": "Temperature",
              "req": {
                "nAdr": 1,
                "param": {
                  "sensorIndexes": [0]
                }
              },
              "returnVerbose": false
            }
          }
        },
        {
          "messaging": "MqttMessaging",
          "message": {
            "mType": "iqrfEmbedOs_Sleep",
            "data": {
              "msgId": "EmbedOsSleep",
              "req": {
                "nAdr": 1,
                "param": {
                  "time": 81,
                  "control": 0
                }
              },
              "returnVerbose": false
            }
          }
        }
      ],
      "timeSpec": {
        "cronTime": [
          "0",
          "*/3",
          "*",
          "*",
          "*",
          "*",
          "*"
        ],
        "periodic": false,
        "period": 0,
        "exactTime": false,
        "startTime": ""
      },
      "persist": true
    },
    "returnVerbose": true
  }
}

Response
--------

{
    "mType": "mngScheduler_AddTask",
    "data": {
        "msgId": "SchedulerAdd",
        "rsp": {
            "clientId": "SchedulerMessaging",
            "taskId": 1512630852
        },
        "insId": "iqdgw01-024220d3db743a88",
        "statusStr": "ok",
        "status": 0
    }
}

8) Getting values every 3 minutes, sent by gateway's scheduler automatically

Responses
---------

{
    "mType": "iqrfEmbedOs_Sleep",
    "data": {
        "msgId": "EmbedOsSleep",
        "rsp": {
            "nAdr": 1,
            "hwpId": 17410,
            "rCode": 0,
            "dpaVal": 103,
            "result": {}
        },
        "status": 0,
        "insId": "iqdgw01-024220d3db743a88"
    }
}

{
    "mType": "iqrfSensor_ReadSensorsWithTypes",
    "data": {
        "msgId": "Temperatures",
        "rsp": {
            "nAdr": 1,
            "hwpId": 17410,
            "rCode": 0,
            "dpaVal": 103,
            "result": {
                "sensors": [
                    {
                        "id": "TEMPERATURE",
                        "type": 1,
                        "name": "Temperature",
                        "shortName": "T",
                        "value": 22,
                        "unit": "°C",
                        "decimalPlaces": 4
                    }
                ]
            }
        },
        "status": 0,
        "insId": "iqdgw01-024220d3db743a88"
    }
}

9) Alternative to polling at 7), IQRF Standard FRC with sleeping, getting values every 3 minutes, sent by gateway's scheduler automatically

Request
-------

{
  "mType": "iqrfSensor_Enumerate",
  "data": {
    "msgId": "testEmbedSensor",
    "req": {
      "nAdr": 1,
      "param": {}
    },
    "returnVerbose": true
  }
}

Response
--------

{
  "mType": "iqrfSensor_Enumerate",
  "data": {
    "msgId": "testEmbedSensor",
    "rsp": {
      "nAdr": 1,
      "hwpId": 17410,
      "rCode": 0,
      "dpaVal": 76,
      "result": {
        "sensors": [
          {
            "id": "TEMPERATURE",
            "type": 1,
            "name": "Temperature",
            "shortName": "T",
            "unit": "°C",
            "decimalPlaces": 4,
            "frcs": [
              144,
              224
            ]
          },
          {
            "id": "BINARYDATA7",
            "type": 129,
            "name": "Binary data7",
            "shortName": "bin7",
            "unit": "?",
            "decimalPlaces": 0,
            "frcs": [
              16,
              144
            ],
            "breakdown": [
              {
                "id": "BINARYDATA7",
                "type": 129,
                "name": "Light indicator",
                "shortName": "light",
                "unit": "%",
                "decimalPlaces": 1
              }
            ]
          },
          {
            "id": "BINARYDATA7",
            "type": 129,
            "name": "Binary data7",
            "shortName": "bin7",
            "unit": "?",
            "decimalPlaces": 0,
            "frcs": [
              16,
              144
            ],
            "breakdown": [
              {
                "id": "BINARYDATA7",
                "type": 129,
                "name": "Potentiometer",
                "shortName": "pot",
                "unit": "%",
                "decimalPlaces": 1
              }
            ]
          }
        ]
      }
    },
    "raw": [
      {
        "request": "01.00.5e.3e.ff.ff",
        "requestTs": "2020-05-06T20:53:47.210+00:00",
        "confirmation": "01.00.5e.3e.ff.ff.ff.00.02.08.02",
        "confirmationTs": "2020-05-06T20:53:47.235+00:00",
        "response": "01.00.5e.be.02.44.00.4c.01.81.81",
        "responseTs": "2020-05-06T20:53:47.571+00:00"
      }
    ],
    "insId": "iqdgw01-024220d3db743a88",
    "statusStr": "ok",
    "status": 0
  }
}

Request
-------

{
  "mType": "mngScheduler_AddTask",
  "data": {
    "msgId": "SchedulerAdd",
    "req": {
      "clientId": "SchedulerMessaging",
      "task": [
        {
          "messaging": "MqttMessaging",
          "message": {
            "mType": "iqrfSensor_Frc",
            "data": {
              "msgId": "EmbedSensors",
              "req": {
                "nAdr": 0,
                "param": {
                  "sensorType": 1,
                  "sensorIndex": 0,
                  "frcCommand": 144,
                  "selectedNodes": [
                    1
                  ],
                  "sleepAfterFrc": {
                    "time": 81,
                    "control": 0
                  },
                    "getExtraResult": true,
                    "extFormat": true
                }
              },
              "returnVerbose": true
            }
          }
        }
      ],
      "timeSpec": {
        "cronTime": [
          "0",
          "*/3",
          "*",
          "*",
          "*",
          "*",
          "*"
        ],
        "periodic": false,
        "period": 0,
        "exactTime": false,
        "startTime": ""
      },
      "persist": true
    },
    "returnVerbose": true
  }
}

Response
--------

{
    "mType": "mngScheduler_AddTask",
    "data": {
        "msgId": "SchedulerAdd",
        "rsp": {
            "clientId": "SchedulerMessaging",
            "taskId": 1404007846
        },
        "insId": "iqdgw01-02420bd3bf4ecc60",
        "statusStr": "ok",
        "status": 0
    }
}
