# Gateway SDK

Build and debug your **IQD-GW04 (Industrial GW)** application using
[Gateway SDK Yocto package](https://dl.iqrf.org/industrial/yocto/iqd-gw-04_yocto-kirkstone_sdk_v15x_20230118.sh).

## Industrial GW devices

The embedded Linux image based on the
[Yocto project](https://www.yoctoproject.org/) is **optional** for the device.
It features same basic set of the software for the IQRF and IP connectivity as
Armbian image, plus support for
[Mender OTA](https://www.mender.io/) **updates and remote management**.
The SD card is mounted as **read-only** which is also important attribute.
The base [image](https://dl.iqrf.org/industrial/yocto/iqd-gw-04_yocto-kirkstone_uart_v150_20230201.sdimg.bz2)
can be extended based on the specific customer requirements.

## SDK installation

```bash
# download
dev@ubuntu:~/iqrf-gw-sdk$ wget https://dl.iqrf.org/industrial/yocto/iqd-gw-04_yocto-kirkstone_sdk_v15x_20230118.sh

# setup
dev@ubuntu:~/iqrf-gw-sdk$ ./iqd-gw-04_yocto-kirkstone_sdk_v15x_20230118.sh
IQRF-GW-OS INDUSTRIAL SDK installer version 1.5.x
=================================================
Enter target directory for SDK (default: /opt/iqrf-gw-os/1.5.x):
You are about to install the SDK to "/opt/iqrf-gw-os/1.5.x". Proceed [Y/n]? y
[sudo] password for spinarr:
Extracting SDK...........................................................done
Setting it up...done
SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
$ . /opt/iqrf-gw-os/1.5.x/environment-setup-armv7vet2hf-neon-poky-linux-gnueabi
dev@ubuntu:~/iqrf-gw-sdk$
```

## GW Daemon build

```bash
dev@ubuntu:~/iqrf-gw-sdk$
# set dev environment
. /opt/iqrf-gw-os/1.5.x/environment-setup-armv7vet2hf-neon-poky-linux-gnueabi

# shape
git clone https://github.com/logimic/shape
cd shape
git checkout v1.1.0
git submodule init && git submodule update
# rapidjson patch
wget https://gitlab.iqrf.org/open-source/meta-iqrf/-/blob/master/meta-iqrf-core/recipes-iqrf/shape/shape/0003-Update-rapidjson-to-8f4c021fa2f1e001d2376095928fc053.patch
git apply 0003-Update-rapidjson-to-8f4c021fa2f1e001d2376095928fc053.patch
./buildMake.sh

# shapeware
git clone https://github.com/logimic/shapeware
cd shapeware
git checkout v1.1.0
git submodule init && git submodule update
./buildMake.sh

# iqrf gw daemon
git clone https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon
cd iqrf-gateway-daemon
git checkout v2.4.0
git submodule init && git submodule update
./buildMake.sh
```

## Customer extension

Adding your application into the base image.

```bash
roota_path="/mnt/rootA"
rootb_path="/mnt/rootB"

# mount image first
loop_device=$( losetup --find --show --partscan ${BASEIMAGE})

mkdir -p ${roota_path} ${rootb_path}

# mount partitions
mount "${loop_device}p2" ${roota_path}
mount "${loop_device}p3" ${rootb_path}

# copy directory structure of customer project to target rootfs
cp -rf ${USERDIR} ${roota_path} ${rootb_path}
sync

# cleanup
losetup -D $loop_device
```
