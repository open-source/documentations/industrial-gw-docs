# OTA updates

Update your **IQD-GW-04 (Industrial gateway) fleet** using [Mender](https://www.mender.io/)
platform in a few minutes.

**Open-source** [Mender server](https://mender.io/plans/faq) is freely available.

Mender also provides for a **remote access** to the device such as remote terminal and
port forwarding.

Possible image updates are:

- single file
- single directory
- multiple directories
- full root filesystem

![Mender open-source server](/images/mender-ota.png)

## Mender server

Our testing server is running here: <https://mender.iqrf.org>

## Industrial GW devices

The embedded Linux image based on the
[Yocto project](https://www.yoctoproject.org/) is **optional** for the device.
It features same basic set of the software for the IQRF and IP connectivity as
Armbian image, plus support for [Mender](https://www.mender.io/) OTA **updates**.
The SD card is mounted as **read-only** which is also important attribute.
The image can be extended based on the specific customer requirements.
