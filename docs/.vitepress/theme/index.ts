import mediumZoom from 'medium-zoom';
import { type Route, useRoute } from 'vitepress';
import { default as DefaultTheme } from 'vitepress/theme';
import { nextTick, onMounted, watch } from 'vue';

import './custom.scss';

export default {
	...DefaultTheme,
	setup(): void {
		const route: Route = useRoute();
		const initZoom = (): void => {
			mediumZoom('.vp-doc img', { background: 'var(--vp-c-bg)' });
		};
		onMounted((): void => {
			initZoom();
		});
		watch(() => route.path, () => nextTick((): void => {
			initZoom();
		}));
	},
};
