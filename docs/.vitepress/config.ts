import { defineConfig } from 'vitepress';

export default defineConfig({
	lang: 'en_US',
	title: 'Industrial IQRF gateway',
	description: 'Documentation for Industrial IQRF gateway IQD-GW04',
	base: '/industrial/',
	head: [
		['link', { rel: 'icon', href: '/industrial/favicon.ico' }],
	],
	outDir: '../dist/',
	lastUpdated: true,
	themeConfig: {
		editLink: {
			pattern: 'https://gitlab.iqrf.org/open-source/industrial-gw-docs/-/blob/master/docs/:path',
		},
		logo: '/logo.svg',
		nav: [
			{
				text: 'Docs',
				link: '/introduction',
				activeMatch: '/*',
			},
			{
				text: 'Product page',
				link: 'https://www.iqrf.org/product-detail/iqd-gw04',
				target: '_blank',
			},
			{
				text: 'E-shop',
				link: 'https://eshop.iqrf.org/p/iqd-gw04-02',
				target: '_blank',
			},
			{
				text: 'Other docs',
				items: [
					{
						text: 'Generic gateway',
						link: 'https://docs.iqrf.org/iqrf-gateway/',
					},
					{
						text: 'IQube gateway',
						link: 'https://docs.iqrf.org/iqube/',
					},
				],
			},
		],
		search: {
			provider: 'local',
		},
		siteTitle: false,
		sidebar: [
			{
				items: [
					{ text: 'News', link: '/news' },
					{ text: 'Introduction', link: '/introduction' },
					{ text: 'Device services', link: '/services' },
				],
			},
			{
				text: 'Getting started',
				items: [
					{ text: 'Fist steps', link: '/getting-started/first-steps' },
					{ text: 'Gateway information', link: '/getting-started/gateway-information' },
					{ text: 'Gateway configuration', link: '/getting-started/gateway-configuration' },
					{ text: 'IQRF network management', link: '/getting-started/iqrf-network-management' },
					{ text: 'IP network management', link: '/getting-started/ip-network-management' },
					{ text: 'Cloud services', link: '/getting-started/cloud-services' },
					{ text: 'Users and API keys', link: '/getting-started/users-and-api-keys' },
				],
			},
			{
				items: [
					{ text: 'Tutorials', link: '/tutorials' },
					{ text: 'Gateway SDK', link: '/sdk' },
					{ text: 'OTA updates', link: '/ota-updates' },
					{ text: 'Advanced settings', link: '/advanced-settings' },
				],
			},
		],
	},
	vite: {
		css: {
			preprocessorOptions: {
				scss: {
					api: 'modern-compiler',
				},
			},
		},
	},
});
