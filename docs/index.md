---
layout: home

hero:
  name: Industrial IQRF gateway IQD-GW04
  text: documentation
  tagline: 'Ready-to-use IQRF Gateway with Ethernet/WiFi/LTE connection.'
  image:
    src: /images/iqd-gw-04_original.png
    alt: Industrial IQRF gateway
  actions:
    - theme: brand
      text: Get Started
      link: /introduction.html
    - theme: alt
      text: Product page
      link: https://www.iqrf.org/product-detail/iqd-gw04
    - theme: alt
      text: E-shop
      link: https://eshop.iqrf.org/p/iqd-gw04-02
---
