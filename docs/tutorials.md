# Tutorials

## Goal

Demonstrate how to gather sensoric data from 2 gateways.
The gateways are connected to MQTT broker running on the server.
MQTT topics are defined to allow communication with multiple gateways.
Gateway's scheduler is used to ask for sensory values from the devices and
results are sent automatically to the broker.
The scheduler can be set up either using webapp/config/scheduler page or via
API calls.
We use API calls in this tutorial since working with multiple gateways.

## Devices

- 1 server
- 2 gateways, IQD-GW-04 device
- 2 sensoric devices, DDC-SE-RE-01 kits

## Architecture

![Architecture](/images/apps-mqtt.png)

## Steps

- Connect gateways to the central server, MQTT broker
- Confirm connection to DPA coordinator in the gateway
- Configure necessary parameters in DPA coordinator
- Bond sensor devices to the network, to DPA coordinator
- Enable and run network enumeration in the gateway's DB
- Check for enumerated sensors in DB
- Configure gateway's scheduler to get sensoric values and put to sleep the devices

### Connect gateways and laptop

#### GW1

![MQTT GWID](/images/webapp-gwid1.png)

![MQTT configuration](/images/webapp-mqtt1.png)

![Daemon restart](/images/webapp-iqrf-restart.png)

![MQTT log](/images/webapp-mqtt-log1.png)

![DB enable](/images/webapp-infodb-enable.png)

#### GW2

![MQTT GWID](/images/webapp-gwid2.png)

![MQTT configuration](/images/webapp-mqtt2.png)

![Daemon restart](/images/webapp-iqrf-restart.png)

![MQTT log](/images/webapp-mqtt-log2.png)

![DB enable](/images/webapp-infodb-enable.png)

#### Laptop

![MQTT client](/images/mqttbox-create-client.png)

![MQTT publish](/images/mqttbox-iqrf-publish.png)

![MQTT subscribe](/images/mqttbox-iqrf-subscribe.png)

## API calls

### GW1 API calls

<<< @/public/examples/apps-api-024220D3DB743A88.txt

### GW2 API calls

<<< @/public/examples/apps-api-02420BD3BF4ECC60.txt
