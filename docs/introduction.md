# Introduction

IQD-GW04 (Industrial) is a generic IQRF® gateway consisting of the [TR](https://iqrf.org/products/transceivers)
module as a DPA coordinator, LTE module, a single-board with Ethernet and WiFi, a pre-configured eMMC flash
and a protective aluminium housing.

This device enables IQRF® customers to implement IQRF® systems with connection to existing servers
such as IBM Cloud, Microsoft Azure, AWS IoT and others.
The source code of [IQRF Gateway daemon](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon)
and [IQRF Gateway webapp](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp)
is provided by the open source GitLab projects under Apache 2.0 licence.

The device can be delivered with [Armbian](https://www.armbian.com/) based Linux
image or with [Yocto](https://www.yoctoproject.org/) based Linux image.
The decision is made by the customer. The key features for both types of images follow.

## Armbian

- Filesystem of eMMC chipset is mounted as read-write
- Hardware GPIO watchdog and RTCC with power backup support
- Logs are kept in memory and rotated to eMMC periodically
- Support for WireGuard VPN connection
- Support for M/Monit monitoring server
- SW updates are handled via Deb packages
- No fleet updates are available
- The customers are free to add any Debian based packages

## Yocto

- Filesystem of eMMC chipset is mounted as read-only with defined overlay
- Hardware GPIO watchdog and RTCC with power backup support
- Logs are kept in memory and can be rotated to eMMC periodically
- Support for WireGuard VPN connection
- SW updates are handled via Mender artifacts
- Fleet updates are available via Mender server (open source) and artifacts
- Built in support for M/Monit monitoring server
- The custom applications must be built with support of our Yocto gateway SDK
- A solution based on the customer's requirements possible

## Parts

- [Raspberry Pi CM4](https://www.raspberrypi.com/products/compute-module-4/) ARM Cortex-A72 quad-core module
- [CM4-IO-BASE-A](https://www.waveshare.com/CM4-IO-BASE-A.htm) Mini Base Board For Compute Module 4
- [KONA-RASP-04](https://www.iqrf.org/product-detail/kona-rasp04-10b) IQRF extension board
- [Quectel EG25-G](https://www.quectel.com/product/lte-eg25-g-mpcie) LTE miniPCIe modem
- Protective aluminium box
- Power supply 3A at 5V connector

## Power off

Use [Gateway button](/getting-started/gateway-information#button) to power-off
the device first, wait 30s to shutdown the device completely, unplug the power supply.

## Technical data

See the product [page](https://www.iqrf.org/product-detail/iqd-gw04) and [sheet](https://www.iqrf.org/download/5d65c980-c240-437e-b386-736f41cb159e).
