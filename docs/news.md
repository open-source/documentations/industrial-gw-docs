# News

## Summary

* Image v1.7.0 (Armbian Bookworm and Yocto Kirkstone)
  * Daemon: Release v2.6.x
  * Daemon: New IQRF standard Light supported
  * Daemon: IQRF standard DALI deprecated
  * Daemon: Standard FRCs reworked to include also MID and HWPID in JSON responses
  * Daemon: Set logging level of deployment configurations to warning
  * Daemon: Autonetwork service always reports new nodes
  * Daemon: IDE counterpart includes GW_ADDR in asynchronous TR packets
  * Daemon: Mqtt messaging sets cleanSession according to QOS value
  * Daemon: Detailed [changes and fixes](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/-/blob/release/v2.6.x/debian/changelog)
  * Webapp: Release v2.6.x
  * Webapp: New IQRF standard Light supported
  * Webapp: IQRF standard DALI components removed
  * Webapp: Mender client configuration fix in Maintenance
  * Webapp: Loading JSON schemas from the filesystem
  * Webapp: Added Syslog logging configuration
  * Webapp: MQTT configuration uses autodetection for TLS usage
  * Webapp: Removed Inteliments InteliGlue MQTT connection creator from Cloud services
  * Webapp: Detailed [changes and fixes](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/-/blob/release/v2.6.x/debian/changelog)
  * Setter: HWPID added in Gateway configuration file
  * Cloud: HWPID added in MQTT topics path configuration
  * Image: Armbian Bookworm build v24.08
  * Image: Yocto Kirkstone build
  * IQRF Gitlab tag: v25.02.0

-- Rostislav Spinar rostislav.spinar@iqrf.com Mon, 03 Mar 2025 20:00:00 +0100

* Image v1.6.0 (Armbian Jammy and Yocto Kirkstone)
  * Daemon: Release v2.5.x
  * Daemon: DALI command included in FRC response
  * Daemon: Standard FRC reworked with little fixes
  * Daemon: IQRF repository cache fix for DALI standard
  * Daemon: Default FRC timeout changed from infinite to 2 minutes
  * Webapp: Release v2.5.x
  * Webapp: Password change for root/admin users enabled in initial setup wizard and SSH service page
  * Webapp: Mender client v4 supported
  * Webapp: Backup manager improved
  * Webapp: Spinner added for reboot and poweroff actions
  * Cloud: IQRF cloud system supported
  * Image: Armbian build v23.11
  * Image: Default Armbian SSH root password set to gateway ID
  * Image: Default Yocto SSH admin password set to gateway ID
  * IQRF Gitlab tag: v24.05.0

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Fri, 10 May 2024 12:35:00 +0200

* Image v1.5.1 (Armbian Jammy and Yocto Kirkstone)
  * Daemon: Release v2.5.x
  * Daemon: IQRF-UDP protocol reworked and completed
  * Daemon: [IQRFPY](https://apidocs.iqrf.org/iqrfpy/latest/iqrfpy.html) library support
  * Webapp: Release v2.5.x
  * Webapp: Mobile connections improved
  * Webapp: Mobile connections LTE watchdog supported
  * Webapp: Monit system checks setting enabled
  * Webapp: System services menu created
  * Webapp: Tuptime log added into gateway diagnostic logs
  * Image: Armbian build v23.11.1
  * Image: Node-RED programming tool added
  * IQRF Gitlab tag: v24.01.0

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 29 Jan 2024 11:15:00 +0100

* Image v1.5.0 (Armbian Jammy and Yocto Kirkstone)
  * Daemon: Release v2.4.x
  * Deamon: Runs after gateway time-synchronization is done or after 2 m timeout
  * Daemon: Address space allocation and MID list extentions for AutoNetwork
  * Daemon: DPA params and Maintanance IQMESH services supported
  * Daemon: Lite DB supported and accessible via IQRF Info API
  * Daemon: Scheduler reworked, support for a task enabling and disabling
  * Daemon: IQRF Repository cache update support
  * Webapp: Release v2.5.x
  * Webapp: Bonding NFC reader added
  * Webapp: Address space allocation and MID list support for AutoNetwork
  * Webapp: Standard manager network enumeration with list of devices table
  * Webapp: IP manager ETH, WiFi, GSM reworked
  * Webapp: Log management improved, Journal dynamic loading supported
  * Webapp: Gateway maintanance supported, Backup and restore, Remote monitoring
  * Webapp: Support for KONA-RASP-04 adapter mapping
  * Webapp: Support for APCUPSD service management
  * Webapp: IQRF Repository cache update support
  * Controller: Release v1.4.x, software extended and improved
  * Setter: Release v1.1.x, software extended and improved
  * MQTT broker: secured by default user: `iqrf`, password: `GWID string`
  * APCUPSD manager: Power management for IQRF UPS device added
  * Mender clients: Included and configurable via Webapp UI
  * Pixla client: Removed, not supported any more
  * Onboard WiFi: Disabled by default, recommended to use USB WiFi dongle
  * Power uUSB changed from USB device to USB host mode to support IQRF UPS
  * Linux kernel: 5.15.x-rpi
  * Armbian LTS Jammy release
  * Yocto application Docker containers supported
  * Yocto LTS Kirkstone release
  * IQRF Gitlab tags: v23.04.0

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 17 Apr 2023 10:00:00 +0100

## Releases

1.7.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Ondřej Hujňák \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-bookworm_uart_v170_20250224.img.gz

* Yocto image:
  * iqd-gw-02_yocto-kirkstone_uart_v170_20250224.sdimg.bz2
  * iqd-gw-02_yocto-kirkstone_uart_v170_20250224.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.6.2)
  * iqrf-gateway-webapp (2.6.3)
  * iqrf-gateway-controller (1.5.0)
  * iqrf-gateway-uploader (1.0.7)
  * iqrf-gateway-setter (1.6.0)
  * iqrf-cloud-provisioning (0.1.2)
  * Armbian updates for LTS Bookworm release
  * Yocto updates for LTS Kirkstone release

-- Rostislav Spinar <rostislav.spinar@iqrf.com> Mon, 03 Mar 2024 20:00:00 +0100

1.6.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-04_armbian-jammy_uart_v160_20240417.img.gz

* Yocto image:
  * iqd-gw-04_yocto-kirkstone_uart_v160_20240525.sdimg.bz2
  * iqd-gw-04_yocto-kirkstone_uart_v160_20240525.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.5.7)
  * iqrf-gateway-webapp (2.5.20)
  * iqrf-gateway-controller (1.5.0)
  * iqrf-gateway-uploader (1.0.7)
  * iqrf-gateway-setter (1.3.0)
  * iqrf-cloud-provisioning (0.1.1)
  * Armbian updates for LTS Jammy release
  * Yocto updates for LTS Kirkstone release

-- Rostislav Spinar <rostislav.spinar@iqrf.com> Mon, 27 May 2024 13:50:00 +0200

1.5.1 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-04_armbian-jammy_uart_v151_20240129.img.gz

* Yocto image:
  * iqd-gw-04_yocto-kirkstone_uart_v151_20240119.sdimg.bz2
  * iqd-gw-04_yocto-kirkstone_uart_v151_20240119.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.5.6)
  * iqrf-gateway-webapp (2.5.16)
  * iqrf-gateway-controller (1.5.0)
  * iqrf-gateway-uploader (1.0.7)
  * iqrf-gateway-setter (1.2.2)
  * Armbian updates for LTS Jammy release
  * Yocto updates for LTS Kirkstone release

-- Rostislav Spinar <rostislav.spinar@iqrf.com> Mon, 29 Jan 2024 11:30:00 +0100

1.5.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondracek \]
\[ Vasek Hanak \]
\[ Marek Belisko \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-04_armbian-bullseye_uart_v150_20230414.img.gz

* Yocto image:
  * iqd-gw-04_yocto-kirkstone_uart_v150_20230424.sdimg.bz2
  * iqd-gw-04_yocto-kirkstone_uart_v150_20230424.mender
  * iqd-gw-04_yocto-kirkstone_sdk_v15x_20230417.sh

* IQRF GW software
  * Daemon: v2.4.3
  * Webapp: v2.5.6
  * Controller: v1.4.2
  * Uploader: v1.0.5
  * Setter: v1.1.5
  * Armbian updates for LTS Jammy release
  * Yocto updates for LTS Kirkstone release
  * Yocto SDK for LTS Kirkstone release

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 24 Apr 2023 09:12:00 +0100
