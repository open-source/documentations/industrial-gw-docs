# Users and API keys

## User manager

User management of the gateway.

![IQRF Gateway Webapp screenshot](/images/webapp-usermanager-page.png)

## API key manager

REST API key management for the gateway.

![IQRF Gateway Webapp screenshot](/images/webapp-keymanager-page.png)

Read more about webapp's REST API [here](https://docs.iqrf.org/iqrf-gateway/user/webapp/api.html).
