# Gateway information

## Knowing IP address

Using any IP address scanner **to search** your network subnet and locate the
gateway IP address.
The gateway hostname is configured to **industrial**.

![IP scanner screenshot](/images/ip-scanner.png)

## Boot and power-off

The gateway **boot process** takes about 30s and it takes additional 10s
before Gateway led described in below chapter *LED operation* is stabilized.

It is recommended to **power-off** the gateway using Gateway button
described in below chapter *Button* and then unplug the power supply.
**It takes about 30s to shutdown the device completely.**
This ensures correct power-off of the device.

## LED operation

![IQD-GW04](/images/iqd-gw04_original_labels.png)

There are 2 two-colour LEDs available on the gateway.
IQRF Coordinator LED and Gateway LED.
Gateway LED follows these rules:

- **Green LED slow** flashing means that IQRF Gateway daemon is
  running in operational/forwarding mode.
- **Green LED fast** flashing means that IQRF Gateway daemon is
  running in service mode.
- **Red LED slow** flashing means that IQRF Gateway daemon service is
  not running.
- **Red LED fast** flashing means that IQRF Gateway daemon interface
  is not correctly configured.

## Button

While button is being pressed green LED is on.

- after 2s of being pressed red led flashes 1x: **selected daemon API call**
  is executed upon button release
- after 6s of being pressed red led flashes 2x: **gateway power off**
  is executed upon button release
- after 10s of being pressed red led flashes 3x: **gateway factory setting**
  is executed upon button release

*Tip*: **Daemon API call for execution upon button release can be
configured in config/controller UI page.**  
*Tip*: **Use the button to power off the gateway gracefully in order to
preserve the SD card.**  
*Note*: **Factory setting does not clear all bonded devices from the
coordinator by default.** See config/controller UI page.

## Log in

Web server is running on **default port 80** by default. Create your **username
and password** when login for the first time or after restoring default
setting using Gateway button.

![IQRF Gateway Webapp screenshot](/images/webapp-setting-adduser.png)

## Information

Information about **installed software, resources and TR module** is well
displayed on the page.
**Download diagnostics** button saves all the necessary logs for the further
analysis.

![IQRF Gateway Webapp screenshot](/images/webapp-info-page.png)

## Log

IQRF gateway **daemon** log is displayed and can be downloaded.

![IQRF Gateway Webapp screenshot](/images/webapp-log-page.png)

## Modes

IQRF gateway daemon can be switched to different modes.

- **Operational**: IQRF JSON API via MQ/MQTT/WS channels are enabled
- **Service**: IQRF via UDP channel is enabled, ready to connect IQRF IDE
  software to the gateway
- **Forwarding**: IQRF JSON API via MQ/MQTT/WS channels are enabled with
  forwarding via UDP as well

![IQRF Gateway Webapp screenshot](/images/webapp-mode-page.png)

IQRF IDE connection to the gateway.

- Switch the gateway daemon to the **Service mode**
- Configure IDE UDP channel, select **IQRF gateway**

![IQRF IDE screenshot](/images/ide-udp-connection.png)

## IQRF services

IQRF Gateway **service** management.

![IQRF Gateway Webapp screenshot](/images/webapp-service-page.png)

## SSH service

SSH service management. The service is disabled by default.

![IQRF Gateway Webapp screenshot](/images/webapp-ssh-page.png)

## Upgrades

Linux packages can be **installed automatically** by the system itself.
This also updates any new packages related to IQRF Gateway SW such as
iqrf-gateway-{daemon, uploader, controller and webapp} from stable
release repository.

Mender artifacts are provided for Yocto based images.

## Power control

The gateway power control for graceful **restart or power off**.

![IQRF Gateway Webapp screenshot](/images/webapp-powercontrol-page.png)
