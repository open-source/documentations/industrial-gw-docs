# IP network management

## Network connections

It is possible to configure IP setting for Ethernet, WiFi and Mobile connections.

- Ethernet connection

![IQRF Gateway Webapp screenshot](/images/webapp-ipethcon.png)

- WiFi connection

![IQRF Gateway Webapp screenshot](/images/webapp-ipwirelesscon.png)

- Mobile connection

LTE **modem watchdog** can be enabled via dedicated button on the page.

![IQRF Gateway Webapp screenshot](/images/webapp-ipmobilecon.png)

IP **ping watchdog** can be enabled via Monit host internet check.

![IQRF Gateway Webapp screenshot](/images/webapp-monit-hostinternet.png)

## WireGuard VPN

[WireGuard](https://www.wireguard.com/quickstart) gateway client configuration.
