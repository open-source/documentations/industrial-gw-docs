# First steps

Basic orientation/steps for the user. Steps very much depend on your use-case.

## IP address

- Get to know DHCP
[IP address](/getting-started/gateway-information#knowing-ip-address) of the gateway
- Or if you share IP subnet with the gateway ping hostname **industrial**

``` bash
ping -4 industrial
```

## Gateway WebUI

- Default gateway HTTP server is running on port 80
- If you share IP subnet with the gateway point the browser to hostname <http://industrial>
- Create your own user during very first login using login guide

![Webapp initial guide screenshot](/images/webapp-guide-page.png)

## LTE SIM setting

- Insert your SIM card into gateway SIM holder
- LTE configuration is set in Mobile connections page

![Mobile connections screenshot](/images/iqd-gw04_mmcon.png)

- Disconnect and edit LTE connection: set your APN and PIN, username, password if needed and save changes
- **LTE watchdog** is recommended to be enabled once the modem is in **connected** state
  - LTE watchdog restarts the modem whenever connection fails

## MQTT setting

### Local MQTT broker (since image v1.5.0)

- Broker has been secured using user and password credentials
- Broker credentials: user:`iqrf` password:`GWID string`

### IQRF gateway daemon

- Set your own MQTT broker and its credentials
- [MQTT config](/getting-started/gateway-configuration#mqtt-messaging) page

## Scheduler setting

- Configure your periodic IQRF JSON
  [requests](https://docs.iqrf.org/iqrf-gateway/user/daemon/api.html)
- [Scheduler config](/getting-started/gateway-configuration#scheduler) page

## Configuration backup

- Save/Export IQRF GW [daemon
  config](/getting-started/gateway-configuration#migration) once
  the configuration is finished
- Save/Export [Scheduler
  setup](/getting-started/gateway-configuration#scheduler) once
  the configuration is finished

## SSH login

- Disabled by default, enable via
[SSH config](/getting-started/gateway-information#ssh-service) page
- **Change the default credentials** via
[SSH config](/getting-started/gateway-information#ssh-service) page
- Default login: Armbian image - `root`/`1234`, Yocto image - `admin`/`admin`

## IQRF IDE connection

- Follow this [guide](https://docs.iqrf.org/iqrf-gateway/user/daemon/service-mode.html)
- And select Mode: **IQRF gateway** for Industrial gateway device
