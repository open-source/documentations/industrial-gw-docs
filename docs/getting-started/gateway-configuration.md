# Gateway configuration

**Editing other parameters than listed below are not necessary!**

Most of the parameters are already well tuned for this gateway.

## SMTP setting

- Set your own SMTP server in order to receive mails from WebUI application
- By default there is no server configured
- Setting is optional

![Webapp SMTP screenshot](/images/webapp-smtp-page.png)

## IQRF Repository

IQRF gateway daemon **periodically downloads** state of
[IQRF repository](https://repository.iqrfalliance.org/doc/) and
**caches all the data**.
Download period can be configured via this page.

![IQRF Gateway Webapp screenshot](/images/webapp-repository-page.png)

## MQTT messaging

IQRF gateway daemon MQTT client configuration.
**Multiple MQTT connections are possible**.
Configuration wizards for the well-known cloud platforms are provided in
Webapp menu Clouds.

![IQRF Gateway Webapp screenshot](/images/webapp-mqtt-page.png)

## WebSocket messaging

IQRF gateway daemon WebSocket server configuration.
**Default port matches setting in Webapp**.

![IQRF Gateway Webapp screenshot](/images/webapp-websocket-page.png)

## UDP messaging

IQRF gateway daemon UDP channel configuration.
**Default ports match setting in IQRF IDE**.

![IQRF Gateway Webapp screenshot](/images/webapp-udp-page.png)

## Scheduler

Using **Add button** any
[JSON request](https://docs.iqrf.org/iqrf-gateway/user/daemon/api.html)
gateway is added as scheduler's task.

![IQRF Gateway Webapp screenshot](/images/webapp-scheduler-page.png)

There are 3 options to schedule task:

- Cron format (e.g. every 5s -\> `*/5* * * * * *`)
- Exact time (one shot task at specific time in format `dd.mm.rrrr mm:ss`)
- Period (periodic task at defined period)

There are 3 options to direct JSON response (result of scheduled JSON
request task) to:

- MqMessaging
- MqttMessaging
- WebsocketMessaging

There is also possibility to **export and then import exported tasks.**

Read more about the scheduler and its tasks [here](https://docs.iqrf.org/iqrf-gateway/user/daemon/scheduler.html).

## Tracer

IQRF gateway daemon logging can be configured via Tracer page.
A communication with IQRF repository has been separated into its own log file.

It is possible **to set log verbosity, sizes and names with or without
timestamp**.
Logs without timestamp overwrite itself after reaching set size.

![IQRF Gateway Webapp screenshot](/images/webapp-tracer-page.png)

## Migration

Migration gives possibility **to handle export and import** of the IQRF gateway
configuration.

![IQRF Gateway Webapp screenshot](/images/webapp-import_export-page.png)

## Controller

IQRF Gateway controller is responsible for the functionality of the gateway
button and LEDs.

Following **parameters** can be configured:

- websocket servers (well configured for default gateway daemon)
- button API call to the gateway daemon (no call, discovery, AutoNetwork)
- logger severity level (trace, debug, ..., error)
- factory reset level (coordinator, daemon, IP network, web application)

**Elaboration on factory reset levels**:

- Coordinator (performs Clear all bonds, sets Access Password to
16x0, sets TX power and RF filter to 7)
- Daemon (default daemon's package configuration is set, any custom
setting is deleted)
- IP network (default image DHCP Ethernet is set, any custom setting is deleted)
- Web application (deletion of all webapp users)

![IQRF Gateway Webapp screenshot](/images/webapp-config-controller.png)
