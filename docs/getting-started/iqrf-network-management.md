# IQRF network management

## Send DPA packet

**Any** DPA packet can be sent using this page.
There are **IDE macros** to select from already predefined DPA packets.

The user can choose a fixed address independently of the address
contained in the macros by enabling *Use custom NADR*.  
An user timeout to wait for DPA response can be use by enabling *Use
custom DPA timeout*.

![IQRF Gateway Webapp screenshot](/images/webapp-senddpa-page.png)

## Send JSON request

**Any** IQRF JSON request can be sent using this page. The best is to open another
tab in browser using button *Show API documentation* and copy any
request from displayed examples on API page.

![IQRF Gateway Webapp screenshot](/images/webapp-sendjson-page.png)

## Coordinator upload

IQRF DPA custom handler (`*.hex`) and plugin (`*.iqrf`) can be uploaded to
the coordinator TR module in the gateway.
IQRF DPA plugin is automatically preselected for the given IQRF OS version
and downloaded from IQRF repository.

To upload any IQRF DPA handler or plugin to the devices in the network
DPA Over The Air upload must be used along with the wizard in IQRF IDE.
Switch gateway to service mode, connect IQRF IDE via UDP and follow these
[steps](https://doc.iqrf.org/IQRF-IDE-Help/window_ntw_manager_control_upload.htm).

**Be aware that DPA plugin upload takes tens of seconds!**

![IQRF Gateway Webapp screenshot](/images/webapp-trupload-page.png)

## TR configuration

TR module **configuration can be shown/changed** using this page.

The coordinator address 0 is selected by default. Using *Read button*
address configuration is read from the given device address 0-239.
The*Write button* writes new configuration to previously selected device or
to coordinator by default.

## Network manager

The network manager can be used to perform necessary tasks in order to
**create IQMESH network** such as device bonding, discovery, ping and
enumeration.
There is a support for the Local bonding e.g. via button and for the Smart
connect via code.

Additional IQMESH services such as **AutoNetwork** and network
**Backup/Restore/OTA/Maintenance** are also provided.

![IQRF Gateway Webapp screenshot](/images/webapp-netmanager-page.png)

## Standard manager

The standard manager can be used to work with IQRF Standard devices such as
IQRF Sensor, Binary output, Light and DALI.

![IQRF Gateway Webapp screenshot](/images/webapp-stdmanager-page.png)
