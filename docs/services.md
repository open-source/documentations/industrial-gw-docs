# Device services

## Default coordinator setting

- DPA UART coordinator plugin, 57600 Bd
- LP networks enabled
- Access Password, 16x0
- Tx power, 7
- Rx filter, 7 (**Advised for ANY IQRF Linux gateway**)

## Default services setting

- [IQRF Gateway daemon](#iqrf-gateway-daemon)
- [IQRF Gateway webapp](#iqrf-gateway-webapp)
- [IQRF Gateway controller](#iqrf-gateway-controller)
- [IQRF Gateway setter](#iqrf-gateway-setter)
- [IQRF Gateway uploader](#iqrf-gateway-uploader)
- [IQRF Cloud provisioning](#iqrf-cloud-provisioning)
- [Mosquitto MQTT broker](#mosquitto-mqtt-broker)
- [Mender OTA update and remote management](#mender-ota-update-and-remote-management)
- [M/Monit monitoring](#mmonit-monitoring)
- [SSH server](#ssh-server)
- [Zeroconf Avahi](#zeroconf-avahi)
- [Network manager](#network-manager)
- [Modem manager](#modem-manager)
- [Time synchronization](#time-synchronization)
- [APCUPSD manager](#apcupsd-manager)

### IQRF Gateway daemon

- MQTT client connected to port 1883
- WebSocket server ports 1338, 1438
- UDP ports 55000 and 55300
- *Tip*: Explore more about its
[configuration](https://docs.iqrf.org/iqrf-gateway/user/daemon/configuration.html)
via web interface.
- *Tip*: Explore more about
[daemon API](https://docs.iqrf.org/iqrf-gateway/user/daemon/api.html).

``` bash
sudo systemctl status iqrf-gateway-daemon.service
```

### IQRF Gateway webapp

- HTTP server is running on port 80.
- *Action*: **Create your own user once logging for the first time, there
  is a guide.**
- *Tip*: Explore IQRF Gateway daemon's possibilities using web interface.

``` bash
sudo systemctl status nginx.service
```

### IQRF Gateway controller

Controls gateway's button and led.

#### v1.1.x

While button is being pressed green LED is on.

- after 2s of being pressed red LED flashes 1x: **selected daemon API call**
  is executed upon button release
- after 6s of being pressed red LED flashes 2x: **gateway power off**
  is executed upon button release
- after 10s of being pressed red LED flashes 3x: **gateway factory
  setting** is executed upon button release

#### v1.0.x

- 1-10s button press **power off** the gateway gracefully.
- 10s button press **restores default** IQRF Gateway daemon's and IP
  configuration and restarts the daemons.
- **Green LED slow** flashing means that the daemon is running in
  operational/forwarding mode.
- **Green LED fast** flashing means that the daemon is running in service mode.
- **Red LED slow** flashing means that the daemon service is not running.
- **Red LED fast** flashing means that the daemon interface is not
  correctly configured.

``` bash
sudo systemctl status iqrf-gateway-controller.service
```

### IQRF Gateway setter

An util that allows to setup the gateway ID and distribute it to all necessary
locations in the file system. It also setups GPIO mapping for the gateway device,
secure MQTT broker, performs backup of configuration files for later restore and
restarts all necessary services.

It is invoked during very first boot and creates a flag in file system that it
has been run already so next boot it exits gracefully if the flag is found.

``` bash
sudo systemctl status iqrf-gateway-setter.service
```

### IQRF Gateway uploader

An util that allows to upload IQRF hex, plugin and TR configuration to the TR
module via SPI interface.

It is invoked and used by the gateway web application.

``` bash
sudo iqrf-gateway-uploader -h
```

### IQRF Cloud provisioning

An util that allows to setup IQRF Gateway daemon MQTT connection to IQRF cloud server.

It is invoked and used by the gateway setter application during initial boot.

``` bash
sudo systemctl status iqrf-cloud-provisioning.service
```

### Mosquitto MQTT broker

MQTT broker is running on port 1883. There is deafult user: iqrf and password: your particular GWID.
Default user and password has been added since image v1.5.0.

- *Tip*: **IQRF Gateway daemon is already configured and connected to it.**
- *Tip*: Check [simple
  examples](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/tree/master/examples/bash)
  for bash.

``` bash
sudo systemctl status mosquitto.service
```

### Mender OTA update and remote management

Having all the gateways at one place with uploading and troubleshooting capabilities? Remote
terminal and port forwarding is possible.

- Login at <https://hosted.mender.io> or set up your own open-source Mender server.
- The client services can be configured and controlled using gateway web interface.

``` bash
sudo systemctl status mender-client.service
sudo systemctl status mender-connect.service
```

### M/Monit monitoring

Having all the gateways at one place with monitoring capabilities?

- Set up your own M/Monit <https://mmonit.com/> server
- The client service can be configured and controlled using gateway web interface.

``` bash
sudo systemctl status monit.service
```

### SSH server

SSH server is by default disabled. When enabled it runs on port 22.

#### lower and v1.5.x

- Default credentials: `root`/`1234` (Armbian image)
- Default credentials: `admin`/`admin` (Yocto image)

#### v1.6.x and higher

- Default credentials: `root`/your particual `GWID` (Armbian image)
- Default credentials: `admin`/your particular `GWID` (Yocto image)

- *Tip*: **Use gateway web (/gateway/ssh) page to enable SSH service if required.**
- *Tip*: Use e.g. Putty
  [client](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
  to log in to the gateway.
- *Action*: **Create your own user once log in for the first time.**

``` bash
sudo systemctl status ssh.service
```

### Zeroconf Avahi

Do not know your IP address from DHCP server within your network?

- *Action:*
  [Zeroconf](https://learn.adafruit.com/bonjour-zeroconf-networking-for-windows-and-linux/overview)
  for Win
- *Tip*: **Ping your device using: ping -4 your-gw-hostname.local**

The hostname can be found either on the gateway label or via gateway web interface
in the information page.

``` bash
sudo systemctl status avahi-daemon.service
```

### Network manager

GW ETH is configured for DHCP IP by default.

- *Tip*: It is possible to **switch to static IP** using gateway web application.

GW Wi-Fi is not configured by default.

- *Tip*: Wi-Fi dongle can be **configured by the customer** using gateway web
  application.

GW GSM is not configured by default.

- *Tip*: GSM dongle can be **configured by the customer** using gateway web
  application.

Wireguard VPN network client can be configured using gateway web application.

``` bash
sudo systemctl status NetworkManager.service
```

### Modem manager

It manages serial link connection to LTE modem. It works together with Network manager to
handle complete LTE functionality. CLI util *mmcli* is available. Modem can be fully
restarted by supported systemd service.

``` bash
sudo systemctl status ModemManager.service
```

### Time synchronization

- Armbian image: systemd-timesyncd client
- Yocto image: systemd-timesyncd client

Keep gateway time in sync.

- *Tip*: **Configure your local timezone** using gateway web application.

``` bash
sudo systemctl status systemd-timesyncd.service
```

### APCUPSD manager

Power management for IQRF UPS device. Service is disabled by default. Service can
be managed using gateway web application.

``` bash
sudo systemctl status apcupsd.service
```
