module.exports = {
	extends: [
		'stylelint-config-standard-scss',
		'stylelint-config-standard-vue',
	],
	rules: {
		'color-hex-length': 'long',
		'selector-class-pattern': '^([a-z][a-z0-9_-]+|VP[A-Za-z]+)$',
	},
};
