import { iqrfEslint } from '@iqrf/eslint-config';

export default iqrfEslint({
	ignores: [
		'.pnpm-store/',
		'dev-dist/',
		'dist/',
	],
});
